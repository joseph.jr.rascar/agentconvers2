import React from 'react'
import { Form, FormGroup,FormControl, ControlLabel, Button, Glyphicon } from 'react-bootstrap'
import {subscribeToEvent, emitEvent, sendRequest, getExpressions} from '../utils/serverhome-api'
import './Football.css';
import {searchRequest} from '../utils/voice-helper'

class Football extends React.Component {
    
    constructor(props){
        super(props);
        this.state = { searchValue: "",
                       shortResult: "",
                       searchGlobalValue: "",
                       messages: "",
                       isTable : false,
                       searchResult: null,
                       handleGlobalChange: null };
        this.handleGlobalChange = this.handleGlobalChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubmitGlobal = this.handleSubmitGlobal.bind(this);
    }

    getResponseData(questionText){
        
        var self= this;
        getExpressions().then((expressions)=>{
            console.log('EXPRESSION')
            console.log(expressions);
            self.setState({"expressions": expressions});
            if(questionText){
                console.log('PRESENT')
                var objRequest = searchRequest(questionText, expressions);
                console.log({"transcript": questionText,
                                "data": objRequest});
                if(objRequest && objRequest.plugin){
                    self.sendData(objRequest);
                }
                    
            }
        });
    }

    sendData(objRequest){
        sendRequest(objRequest.plugin, objRequest.action, objRequest.data).then((data)=>{

            console.log({"response":data.resultText});
            this.updateMessagesResponse(data.resultText);
        });
    }
    
    handleChange (event) {
       this.setState({
            searchValue: event.target.value
        });
    }

    handleGlobalChange (event) {
        console.log(this.state.searchGlobalValue);
        this.setState({
             searchGlobalValue: event.target.value
         });
     }

    addZero(i) {
        if (i < 10) {
          i = "0" + i;
        }
        return i;
      }

     updateMessages(message){
        var d = new Date();
        var h = this.addZero(d.getHours());
        var m = this.addZero(d.getMinutes());

        let hour = h + ":" + m;

         this.setState({
             messages: 
             this.state.messages + 
             '<li style="width:100%">' +
                    '<div class="msj macro">' +
                    '<div class="avatar"><img class="img-circle" style="width:100%;" src="" /></div>' +
                        '<div class="text text-l">' +
                            '<p>'+ message +'</p>' +
                            '<p><small>'+hour+'</small></p>' +
                        '</div>' +
                    '</div>' +
                '</li>'
         })
     }

     updateMessagesResponse(message){
        var d = new Date();
        var h = this.addZero(d.getHours());
        var m = this.addZero(d.getMinutes());

        let hour = h + ":" + m;

        if(message.length < 1){
            console.log('okok empty');
        }
        console.log('tes grands morts')
        if(message.length < 1){
            console.log('hello death');
            message = 'Je ne comprends pas...';
        }

         this.setState({
             messages: 
             this.state.messages + 
             '<li style="width:100%;">' +
             '<div class="msj-rta macro">' +
                 '<div class="text text-r">' +
                     '<p>'+message+'</p>' +
                     '<p><small>'+hour+'</small></p>' +
                 '</div>' +
             '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="" /></div>' +                                
            '</li>'
         })
     }
    
    handleSubmit (event) {
        console.log("emit event footballSearch : "+this.state.searchValue);
        emitEvent("footballsearch", this.state.searchValue);
        var self= this;
        sendRequest("football", "clubcoach", {searchValue: this.state.searchValue}).then((data)=>{
            if(data.resultText){
                var utterThis = new SpeechSynthesisUtterance(data.resultText);
                utterThis.lang = 'fr-FR';
                console.log({"response":data.resultText});
                window.speechSynthesis.speak(utterThis);
                self.setState({
                    shortResult: data.resultText
                });
            }
        });
        if(event)
            event.preventDefault();
    }

    handleSubmitGlobal(event){
        console.log("new global emit: " + this.state.searchGlobalValue);
        this.getResponseData(this.state.searchGlobalValue);
        this.updateMessages(this.state.searchGlobalValue);

        if(event)
            event.preventDefault();
    }
    
    componentDidMount(){
        var self= this;
        subscribeToEvent("footballresult", function(club){
            console.log(club);
            self.setState({
                searchResult: 
                "<img border='0' alt='' src='"+ club.logo +"' width='100' height='100'></img>" +
                "<b><p>" + club.name + "</p></b>" +
                "<b><p> Position : </b>" + club.position + "ème</p>" +
                "<b><p> Matchs joués : </b>" + club.playedGames + " matchs</p>",
                isTable: club.isTable
            });
        });
        var lastPart = window.location.href.split("/").pop();
        if(lastPart !== "football"){
            // eslint-disable-next-line
            this.state.searchValue= lastPart;
            this.handleSubmit(null);
            this.handleSubmitGlobal(null);
        }
    }

    
    render() {
        var result = this.state.searchResult ? 
                        (this.state.isTable ?
                        <table dangerouslySetInnerHTML={{ __html: this.state.searchResult }} /> :
                        <div dangerouslySetInnerHTML={{ __html: this.state.searchResult }} />)
                    : "";

        console.log(result);
        return (
            <div className='plugincontent plugin-football'>
        
                {/* <Form onSubmit={this.handleSubmit} inline>
                    <FormGroup controlId="formInlineName">
                        <ControlLabel>Search</ControlLabel>{' '}
                        <FormControl type="text" placeholder="terms" value={this.state.searchValue} onChange={this.handleChange} />
                    </FormGroup>{' '}
                    <Button type="submit"><Glyphicon glyph="search" /> </Button>
                </Form>
                <div className="shortResult">
                    <cite>{this.state.shortResult}</cite>
                </div> 
                <div className="result">
                    {result}
                </div> 

                <Form onSubmit={this.handleSubmitGlobal} inline>
                    <FormGroup controlId="formInlineName">
                        <ControlLabel>Search</ControlLabel>{' '}
                        <FormControl type="text" placeholder="terms" value={this.state.searchGlobalValue} onChange={this.handleGlobalChange} />
                    </FormGroup>{' '}
                    <Button type="submit"><Glyphicon glyph="search" /> Envoyer</Button>
                </Form> */}

                <div className="col-sm-3 col-sm-offset-4 frame">
                    <ul>
                        <div dangerouslySetInnerHTML={{ __html: this.state.messages }}></div>
                    </ul>
                    <div>
                        
                        <Form onSubmit={this.handleSubmitGlobal} >
                            <div className="msj-rta macro">
                            <input className="text text-r mytext" style={{ background: 'whitesmoke !important' }}  placeholder="Type a message" value={this.state.searchGlobalValue} onChange={this.handleGlobalChange}/>
                            </div>
                        </Form>
                                
                       

                        <Form onSubmit={this.handleSubmitGlobal} >
                            <div style={{ padding: 10 }}>
                                <Button className="glyphicon glyphicon-share-alt" type="submit"></Button>
                            </div>
                        </Form>   
                    </div>
                </div>  
            </div>  
        );
    }
};

export default Football;