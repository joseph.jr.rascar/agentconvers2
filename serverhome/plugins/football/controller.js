const request = require('request');
const fs = require('fs');

function getNextMatchInformation(clubNameRaw, res){
    let clubSearch = clubNameRaw;

    let nextMatch;
    let nextTeam;
    var options = {
        url: '',
        headers: {
            'User-Agent': 'request',
            'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
        }
    };

    return new Promise((resolve, reject) => {

        resolve(getClubId(clubSearch));
        
    }).then((club)=>{
        options.url = "https://api.football-data.org/v2/teams/"+ club.id +"/matches?status=SCHEDULED"
        
        request(options, ((error, response, body)=>{
            nextMatch = JSON.parse(response.body);
            nextTeam = nextMatch.matches[0].awayTeam.name;
        }));

        return new Promise((resolve, reject) => {
            request(options, ((error, response, body)=>{
                nextMatch = JSON.parse(response.body);
                if(club.name == nextMatch.matches[0].awayTeam.name){
                    nextTeam = nextMatch.matches[0].homeTeam.name
                }else{
                    nextTeam = nextMatch.matches[0].awayTeam.name;
                }
                resolve(nextTeam);
            }));
            
        }).then((result)=>{
            res.end(JSON.stringify({resultText: result}));
        })
    })

}

function getClubPosition(teamName, res){
    let club = getClubInformations(teamName)
        .then((club)=>{
            res.end(JSON.stringify({resultText: club.position + "ème"}));
        });
}

function getClubPosition(teamName, res){
    let club = getClubInformations(teamName)
        .then((club)=>{
            res.end(JSON.stringify({resultText: club.position + "ème"}));
        });
}

function getClubPoints(teamName, res){
    let club = getClubInformations(teamName)
        .then((club)=>{
            res.end(JSON.stringify({resultText: club.points + " points"}));
        });
}

function getClubVictorys(teamName, res){
    let club = getClubInformations(teamName)
        .then((club)=>{
            res.end(JSON.stringify({resultText: club.won + " victoires"}));
        });
}

function getClubDefeats(teamName, res){
    let club = getClubInformations(teamName)
        .then((club)=>{
            res.end(JSON.stringify({resultText: club.lost + " défaites"}));
        });
}

function getClubCoach(teamName, res){

    let options = {
        url: 'https://api.football-data.org/v2/teams/',
        headers: {
            'User-Agent': 'request',
            'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
        }
    };

    getClubId(teamName)
        .then((team)=>{
            return new Promise((resolve, reject)=>{
                options.url += team.id;

                request(options, ((error, response, body)=>{
                    let club = JSON.parse(response.body);
                    let squad = club.squad;
                    if(squad){
                        squad.forEach((player)=>{
                            if(player.role == "COACH"){
                                resolve(player.name);
                            }
                        })
                    }
                }));
            }).then((result)=>{
                res.end(JSON.stringify({resultText: "L'entraineur de " + teamName + " est " + result}));
            });
        })
}

function getClubInformations(teamName){

    let options = {
        url: 'https://api.football-data.org/v2/competitions/2021/standings?standingType=TOTAL',
        headers: {
            'User-Agent': 'request',
            'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
        }
    };
    console.log("je pass ici");
    return new Promise((resolve, reject)=>{
        request(options, ((error, response, body)=>{
            let clubsRaw = JSON.parse(response.body);
            //RAJOUT CAUSE BUG ?
            if(clubsRaw.standings[0] !== undefined){
                let clubs = clubsRaw.standings[0].table;
                clubs.forEach((club)=>{
                    let persoName2= club.team.name.split(" ")[0] + " " + club.team.name.split(" ")[1];
                    let persoName1= club.team.name.split(" ")[0];
                    if(teamName.toUpperCase() == persoName1.toUpperCase() || teamName.toUpperCase() == persoName2.toUpperCase()){
                        resolve(club);
                    }
                })
            }
        }));
    });

}

function getClubId(teamName){
    let teamId;
    let options = {
        url: 'http://api.football-data.org/v2/teams?name='+teamName,
        headers: {
            'User-Agent': 'request',
            'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
        }
    };
    
    return new Promise((resolve, reject) => {
        request(options, ((error, response, body)=>{
            let team = JSON.parse(response.body);
            teamId = team.teams[0].id;

            
            team.teams.forEach((element)=>{
                let persoName2 = element.name.split(" ")[0] + " " + element.name.split(" ")[1];
                
                if(element.shortName.toUpperCase() == teamName.toUpperCase() || persoName2.toUpperCase() == teamName.toUpperCase()){
                    teamId = element.id;
                    resolve({'id':teamId, 'name': element.name});
                }
            })

            
        }));
        
    }).then((result)=>{
        return result;
    });
}

class FootballController {
        
    constructor(io){
        this.io = io;
        io.sockets.on('connection', function(socket){ 
            socket.on('footballsearch', function(searchvalue){
                console.log("footballsearch "+searchvalue);

                getClubInformations(searchvalue)
                    .then((club)=>{
                        socket.emit('footballresult',{"name": club.team.name, "position": club.position, "logo": club.team.crestUrl, "playedGames": club.playedGames});
                    });
            });
        });
    }

    getView(req, res){
        var dataView = { 
            "type" : "Football"
        };
        res.end(JSON.stringify(dataView));
    }

	postAction(req, res){
        switch(req.params.actionId){
            case "nextmatchof":
                getNextMatchInformation(req.body.searchValue, res);
                break;
            case "clubclassement":
                getClubPosition(req.body.searchValue, res);
                break;
            case "clubpoints":
                getClubPoints(req.body.searchValue, res);
                break;
            case "victorynumber":
                console.log('Je passe par ici');
                console.log(req.params.actionId)
                getClubVictorys(req.body.searchValue, res);
                break;
            case "defeatnumber":
                getClubDefeats(req.body.searchValue, res);
                break;
            case "clubcoach":
                getClubCoach(req.body.searchValue, res);
                break;
            case "all":
                console.log('je suis dead')
                res.end(JSON.stringify({resultText: "Je n'ai pas la répoinse dsl"}));               
            default:
            console.log('je suis dead')
                res.end(JSON.stringify({resultText: "Je n'ai pas la répoinse dsl"}));
                break;
            
        }
    }
}

module.exports = FootballController;
